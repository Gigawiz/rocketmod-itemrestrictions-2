﻿using Rocket.API.Collections;
using Rocket.Core;
using Rocket.Core.Logging;
using Rocket.Core.Plugins;
using Rocket.Unturned.Chat;
using Rocket.Unturned.Enumerations;
using Rocket.Unturned.Events;
using Rocket.Unturned.Items;
using Rocket.Unturned.Player;
using SDG.Unturned;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace RocketMod_ItemRestriction
{
    public class PluginIR : RocketPlugin<IR_Config>
    {
        public static PluginIR Instance;
        public string version = "1.0.0.0";

        public override TranslationList DefaultTranslations
        {
            get
            {
                return new TranslationList
                {
                    { "item_notPermitted", "Item not permitted: {0}" },
                    { "translation_version_dont_edit", "1" }
                };
            }
        }

        protected override void Load()
        {
            PluginIR.Instance = this;
            UnturnedPlayerEvents.OnPlayerInventoryAdded += IR_ItemAdded;
            

            Logger.LogWarning("Setting up Item Restrictions by LeeIzaZombie. v" + version);
            Logger.LogWarning("--");
            int count = 0;
            foreach (Item item in this.Configuration.Instance.Items)
            {
                count++;  
            }
            Logger.LogWarning("Black listed items found: " + count);
            Logger.LogWarning("--");
            Logger.LogWarning("Item Restrictions is ready!");
        }

        private void IR_ItemAdded(UnturnedPlayer player, InventoryGroup inventoryGroup, byte inventoryIndex, ItemJar P)
        {
            if (player.IsAdmin && this.Configuration.Instance.ignoreAdmin)
                return;

            if (!R.Permissions.HasPermission(player, "ir.safe"))
            {
                foreach (Item item in this.Configuration.Instance.Items)
                {
                    if (item.ID == P.Item.ItemID)
                    {
                        for (byte page = 0; page < PlayerInventory.PAGES; page++)
                        {
                            byte itemCount = player.Player.inventory.getItemCount(page);
                            for (byte index = 0; index < itemCount; index++)
                            {
                                if (player.Player.inventory.getItem(page, index).Item.ItemID == P.Item.ItemID)
                                {
                                    UnturnedChat.Say(player, Translate("item_notPermitted", UnturnedItems.GetItemAssetById(P.Item.ItemID).name), Color.red);
                                    player.Player.inventory.removeItem(page, index);
                                }
                            }
                        }
                    }
                }
            }
        }

        protected override void Unload()
        {
            UnturnedPlayerEvents.OnPlayerInventoryAdded -= IR_ItemAdded;
        }
    }
}
